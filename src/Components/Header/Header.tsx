import styles from "./Header.module.css";


function Header() {
    return (
      <div>
        <div className={styles.menu}>
            <ul className={styles.menuList}>
                <li>Star Wars</li>
                <li>Home</li>
                <li>About</li>
                <li>Log out</li>
            </ul>
       </div>
      </div>
    );
  };
  


export default Header