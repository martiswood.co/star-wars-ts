import styles from "./LeftContainer.module.css";
import img1 from "../images/storm.png";

function LeftContainer() {
  return (
    <div className={styles.leftContainer}>
      <div className={styles.logo}>
        <img src={img1} alt="" />
      </div>
      <div className={styles.leftContBox}>
        <div className={styles.info}>
          <ul className={styles.infolist}>
            <li>Name Surname </li>
            <li>Nickname</li>
            <li>Email</li>
            <li>Place</li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default LeftContainer;
