import React, { useEffect, useState } from "react";
import styles from "./Main.module.css";
import Pagination from "../Pagination";
import LeftContainer from "../LeftContainer/LeftContainer";
import RightContainer from "../RightContainer";

type State = {
  error: string |null;
  isLoaded: boolean;
  items: Array<User>;
  selectedInfo: null|User;
  categoryName: string;
  allItems:number;
}

const Main = () => {
  const [currentPage, setCurrentPage] = useState(1);

  const [state, setState] = useState<State>({
    error: null,
    isLoaded: false,
    items: [],
    selectedInfo: null,
    categoryName: "people",
    allItems: 0,
  });

  // service
  const fetchPeople = () => {
    const url = `https://swapi.dev/api/${state.categoryName}?page=${currentPage}`;

    fetch(url)
      .then((response) => response.json())
      .then(
        (response) => {
          setState((prevState) => {
            return {
              ...prevState,
              isLoaded: true,
              items: response.results,
              allItems: response.count,
            }
          });
        },
        (error) => {
          setState((prevState) => {
            return {
              ...prevState,
              isLoaded: true,
              error,
            }
          });
        }
      );
  };

  const handleItemClick = (item:User|null) => {
    setState((cloneState) => {
      return {
        ...cloneState,
        selectedInfo:item
      }
    });
  };

  useEffect(() => {
    fetchPeople();
  }, [currentPage]);

  return (
    <div className={styles.mainContainer}>
      <LeftContainer />
      <div className={styles.RightContainerWrapper}>
        <RightContainer
          selectedInfo={state.selectedInfo}
          error={state.error}
          items={state.items}
          handleItemClick={handleItemClick}
        />

        <Pagination allItems={state.allItems} setCurrentPage={setCurrentPage} currentPage={currentPage}/>
      </div>
    </div>
  );
};

export default Main;
