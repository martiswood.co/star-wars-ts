import styles from "./Pagination.module.css";

type Props = {
currentPage: number;
setCurrentPage: (page:number) => void;
allItems: number;
}

const itemsPerPage = 10;

function Pagination(props:Props) {
  const { currentPage, setCurrentPage } = props;

  if (!props.allItems) {
    return null;
  }

  const totalPage = Math.ceil(props.allItems / itemsPerPage);
  const isNextPageAvailable = currentPage < totalPage
  const isPrevPageAvailable  = currentPage > 1

  const buttons = [];

  const onNextPage = () => {
    if (isNextPageAvailable) {
      setCurrentPage(currentPage + 1);
    }
  };

  const onPrevPage = () => {
    if (isPrevPageAvailable) {
      setCurrentPage(currentPage - 1);
    }
  };

  for (let i = 1; i <= totalPage; i++) {
    buttons.push(
      <button key={i} className={styles.button} onClick={() => setCurrentPage(i)}>
        {i}
      </button>
    );
  }

  //disable prev and next buttons consider to the currentpage
  return (
    <div className={styles.pagesMenu}>
      <button className={styles.button} onClick={() => setCurrentPage(1)}>{"<<"}</button>
      <button className={styles.button} onClick={onPrevPage}>
        {"<"}
      </button>
      {buttons}
      <button className={styles.button} onClick={onNextPage}>
        {">"}
      </button>
      <button className={styles.button} onClick={() => setCurrentPage(totalPage)}>{">>"}</button>
    </div>
  );
}

export default Pagination;
