import React, { useState } from "react";
import styles from "./RightContainer.module.css";
import RightContainerList from "./RightContainerList";
import RightContainerTableHeader from "./RightContainerTableHeader";
import RightContainerTableBody from "./RightContainerTableBody";

type Props = {
  selectedInfo: null | User;
  error: string | null;
  items: Array<User>;
  handleItemClick: (item: User | null) => void;
};

function RightContainer(props: Props) {
  const { selectedInfo, error, items, handleItemClick } = props;

  return (
    <>
      {selectedInfo ? (
        // arandzin component
        <RightContainerList
          selectedInfo={selectedInfo}
          handleItemClick={handleItemClick}
        />
      ) : (
        <div className={styles.rightContainer}>
          {/* arandzinn comp */}
          <RightContainerTableHeader />

          {error ? "ERROR" : null}
          {items.length !== 0 ? (
            // component
            <RightContainerTableBody
              items={items}
              handleItemClick={handleItemClick}
            />
          ) : (
            "Loading..."
          )}
        </div>
      )}
    </>
  );
}

export default RightContainer;
