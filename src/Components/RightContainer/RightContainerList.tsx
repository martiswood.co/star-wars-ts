import React from "react";
import styles from "./RightContainer.module.css";

type Props = {
  selectedInfo: User;
  handleItemClick: (item:User|null) => void;
}

const RightContainerList = (props:Props) => {
  const { handleItemClick, selectedInfo } = props;

  const {
    name,
    eye_color,
    birth_year,
    gender,
    hair_color,
    height,
    skin_color,
    mass,
  } = selectedInfo;

  return (
    <div className={styles.rightContainer}>
      <button onClick={() => handleItemClick(null)}>Back</button>
      <ul>
        <li>Name-{name}</li>
        <li>Eye color-{eye_color}</li>
        <li>Birdth-{birth_year}</li>
        <li>Gender-{gender}</li>
        <li>Hair-{hair_color}</li>
        <li>Height-{height}</li>
        <li>Skin-{skin_color}</li>
        <li>Mass-{mass}</li>
      </ul>
    </div>
  );
};

export default RightContainerList;
