import React from "react";
import styles from "./RightContainer.module.css";

type Props = {
  items:Array<User>;
  handleItemClick: (item:User|null) => void;
}

const RightContainerTableBody = (props:Props) => {
  const { items, handleItemClick } = props;

  return (
    <ul className={styles.menuList}>
      {items.map((item) => (
        <li onClick={() => handleItemClick(item)} key={item.name}>
          {item.name}
        </li>
      ))}
    </ul>
  );
};

export default RightContainerTableBody;
