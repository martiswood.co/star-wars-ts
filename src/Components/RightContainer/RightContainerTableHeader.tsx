import React from 'react';
import styles from "./RightContainer.module.css";

const RightContainerTableHeader = () => {
    return (
        <div className={styles.menu}>
            <div>
              <ul className={styles.menuUl}>
                <li>Peoples</li>
                <li>Planets</li>
                <li>Spacies</li>
                <li>Starships</li>
                <li>Vehicles</li>
              </ul>
            </div>
          </div>
    );
}
 
export default RightContainerTableHeader;
