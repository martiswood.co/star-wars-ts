type User = {
    name: string;
    eye_color: string;
    birth_year: number;
    gender: string;
    hair_color: string;
    height: number;
    skin_color: string;
    mass: string;
}